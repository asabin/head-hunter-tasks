package infinite.sequence;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Бесконечная последовательность
 *
 * Возьмём бесконечную цифровую последовательность, образованную склеиванием последовательных положительных чисел: S = 123456789101112131415...
 * Определите первое вхождение заданной последовательности A в бесконечной последовательности S (нумерация начинается с 1).
 *
 * Пример входных данных:
 * 6789
 * 111
 *
 * Пример выходных данных:
 * 6
 * 12
 *
 * Идея:
 * Идея заключается в том, чтобы генерировать последовательность частями с определенным шагом и искать подпоследовательность в каждой новой части.
 * Эти действия повторяются до тех пор, пока искомая подпоследовательность не будет найдена.
 *
 * Инструкция:
 * После запуска введите искомую подпоследовательность.
 */

public class Main {

    private static int STEP = 100;

    private int item;
    private String sequence;

    private int stepNumber = 0;
    private String previousItemEnd = null;

    public static void main(String[] args) {
        System.out.println("Enter a subsequence:");

        Scanner scanner = new Scanner(System.in);
        String subSequence = scanner.nextLine();

        if (validate(subSequence)) {
            Main instance = new Main();
            System.out.println(instance.find(subSequence));
        } else {
            System.out.println("Input error!");
        }
    }

    private int find(String subSequence) {
        item = 1;
        sequence = "";

        if (subSequence.length() > sequence.length()) {
            fillSequence(subSequence.length());
        }

        if (subSequence.equals(sequence)) {
            return 1;
        }

        int index = 0;
        int endSequenceLength = subSequence.length() - 1;

        while (index == 0) {
            sequence = sequence.substring(sequence.length() - endSequenceLength);
            fillSequenceWithStep();

            int wrongEndLength = sequence.length() - STEP;
            if (wrongEndLength != 0) {
                fixSequence(wrongEndLength);
            }

            index = indexOf(sequence, subSequence);
        }

        return (STEP - endSequenceLength) * (stepNumber - 1) + index + 1;
    }

    private void fillSequence(int sequenceLength) {
        while (sequence.length() + String.valueOf(item).length() <= sequenceLength) {
            if (previousItemEnd != null) {
                sequence += previousItemEnd;
                previousItemEnd = null;
            } else {
                sequence += String.valueOf(item);
                item++;
            }
        }
    }

    private void fillSequenceWithStep() {
        fillSequence(STEP);
        stepNumber++;
    }

    private void fixSequence(int wrongEndLength) {
        if (wrongEndLength > 0) {
            sequence = sequence.substring(sequence.length() - wrongEndLength);
        }
        if (wrongEndLength < 0) {
            sequence += String.valueOf(item).substring(0, Math.abs(wrongEndLength));
            previousItemEnd = String.valueOf(item).substring(Math.abs(wrongEndLength));
            item++;
        }
    }

    private int indexOf(String sequence, String subSequence) {
        return sequence.indexOf(subSequence) + 1;
    }

    public static boolean validate(String subSequence) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(subSequence);
        return !matcher.find();
    }
}