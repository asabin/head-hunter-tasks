package minimum.distance;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Минимальное расстояние
 *
 * Дан набор из N точек на плоскости (для простоты можно считать, что у всех точек целочисленные координаты).
 * Найдите минимальное расстояние между двумя точками из этого набора.
 *
 * Пример входных данных:
 * 10 10
 * 20 10
 * 20 15
 *
 * Пример выходных данных:
 * 5
 *
 * Идея:
 * Идея заключается в том, чтобы рекуррентно делить весь массив точек на две части до тех пор, пока в каждой из них не останется минимальное количество точек.
 * Затем в каждой из частей методом перебора ищется пара точек с минимальным расстоянием, после чего минимальные расстояния из каждой части сравниваются между собой.
 * В итоге, все части соединяются по определенному алгоритму до тех пор, пока не будет найдена пара точек с минимальным расстоянием.
 *
 * Инструкция:
 * После запуска введите адрес текстового файла, в котором в каждой строке хранятся целочисленные координаты для одной точки через пробел (X Y).
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("Enter a file path: ");

        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();

        List<Point> points = new ArrayList<>();

        try (FileInputStream fileInputStream = new FileInputStream(path)) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            bufferedReader.lines().forEach(string -> points.add(
                    new Point(Integer.parseInt(string.split(" ")[0]), Integer.parseInt(string.split(" ")[1]))));

            System.out.println(minDistPair(points).getDistance());
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        } catch (IOException e) {
            System.out.println("Input/output error!");
        }
    }

    private static Pair minDistPair(List<Point> points) {
        if (points.size() <= 3) return bruteForceMinDist(points);

        sortByX(points);

        int middlePosition = calculateMiddlePosition(points.size());

        List<Point> firstHalfPoints = points.subList(0, middlePosition + 1);
        List<Point> secondHalfPoints = points.subList(middlePosition + 1, points.size());

        Point middlePoint = points.get(middlePosition);

        Pair firstMinDistPair = minDistPair(firstHalfPoints);
        Pair secondMinDistPair = minDistPair(secondHalfPoints);

        Pair minDistPair = firstMinDistPair.getDistance() <= secondMinDistPair.getDistance()
                ? firstMinDistPair
                : secondMinDistPair;

        double minDist = minDistPair.getDistance();

        List<Point> minDistPoints = new ArrayList<>();

        for (Point point : points) {
            if (Math.abs(point.x - middlePoint.x) < minDist)
                minDistPoints.add(point);
        }

        sortByY(minDistPoints);

        for (int i = 0; i < minDistPoints.size() - 1; i++) {
            for (int j = i + 1; j < minDistPoints.size(); j++) {
                if (minDistPoints.get(j).y - minDistPoints.get(i).y >= minDist) break;

                if (minDistPoints.get(i).distance(minDistPoints.get(j)) < minDist) {
                    minDistPair = new Pair(minDistPoints.get(i), minDistPoints.get(j));
                    minDist = minDistPair.getDistance();
                }
            }
        }

        return minDistPair;
    }

    private static Pair bruteForceMinDist(List<Point> points) {
        Pair minDistPair = new Pair(points.get(0), points.get(1));

        if (points.size() == 2) return minDistPair;

        for (int i = 1; i < points.size() - 1; i++) {
            for (int j = i + 1; j < points.size(); j++) {
                if (points.get(i).distance(points.get(j)) < minDistPair.getDistance())
                    minDistPair = new Pair(points.get(i), points.get(j));
            }
        }

        return minDistPair;
    }

    private static void sortByX(List<Point> points) {
        points.sort((point1, point2) -> {
            Integer x1 = point1.x;
            Integer x2 = point2.x;
            return x1.compareTo(x2);
        });
    }

    private static void sortByY(List<Point> points) {
        points.sort((point1, point2) -> {
            Integer y1 = point1.y;
            Integer y2 = point2.y;
            return y1.compareTo(y2);
        });
    }

    private static int calculateMiddlePosition(int size) {
        return size % 2 == 0 ? size / 2 - 1 : size / 2;
    }
}