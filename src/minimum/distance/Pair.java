package minimum.distance;

public class Pair {

    private Point point1;
    private Point point2;
    private double distance;

    public Pair(Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
        this.distance = point1.distance(point2);
    }

    public double getDistance() {
        return distance;
    }

    public String toString() {
        return "Distance (" + point1 + ", " + point2 + ") = " + distance;
    }
}