package minimum.distance;

public class Point {

    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(Point that) {
        return Math.hypot(that.x - this.x, that.y - this.y);
    }

    public String toString() {
        return "Point {" + x + ", " + y + "}";
    }
}